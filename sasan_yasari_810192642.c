#include <stdio.h>
#include <stdlib.h>    
#include <time.h>
#include <string.h>
#define SIZE 4
void show(int arr[][SIZE]){//showing the table
	int i,j;
	printf("---------------------------------\n");//first line of table
	for(i=0;i<SIZE;i++){
		printf("|       |       |       |       |\n");//Columns of table
			for(j=0;j<SIZE;j++){
				printf("|");
				if(arr[i][j]==0)
					printf("       ");
				else {
					if(arr[i][j]/10==0)
						printf("   %d   ",arr[i][j]);
					else if(arr[i][j]/100==0)
						printf("   %d  ",arr[i][j]);
					else if(arr[i][j]/1000==0)
						printf("  %d  ",arr[i][j]);
					else 
						printf("  %d ",arr[i][j]);
				}
			}
			printf("|\n");
			printf("|       |       |       |       |");
			printf("\n---------------------------------\n");
		}

}
int up(int arr[][SIZE]){
	int i,j,l,score=0;
	for(i=SIZE-1;i>0;i--){//take numbers up
		for(j=0;j<SIZE;j++){
			if(arr[i-1][j]==0){
				arr[i-1][j]=arr[i][j];
				arr[i][j]=0;
				for(l=0;l<SIZE-1-i;l++){//shifting numbers
					arr[i+l][j]=arr[i+l+1][j];
					arr[i+l+1][j]=0;
				}
			}
		}
	}
	for(i=0;i<SIZE-1;i++){
		for(j=0;j<SIZE;j++){
			if(arr[i][j]==arr[i+1][j]){//merge numbers
				arr[i][j]*=2;
				score+=arr[i][j];
				arr[i+1][j]=0;
				for(l=1;l<3-i;l++){//shifting numbers
					arr[i+l][j]=arr[i+l+1][j];
					arr[i+l+1][j]=0;
				}
			}
		}
	}
	return score;//result of merged numbers
}
int down(int arr[][SIZE]){
	int i,j,l,score=0;
		for(i=0;i<SIZE-1;i++){//take numbers down
			for(j=0;j<SIZE;j++){
				if(arr[i+1][j]==0){
					arr[i+1][j]=arr[i][j];
					arr[i][j]=0;
					for(l=0;l<i;l++){//shifting numbers
						arr[i-l][j]=arr[i-l-1][j];
						arr[i-l-1][j]=0;
					}
				}
			}
		}
	for(i=SIZE-1;i>0;i--){
		for(j=0;j<SIZE;j++){//merge numbers
			if(arr[i-1][j]==arr[i][j]){
				arr[i][j]*=2;
				score+=arr[i][j];
				arr[i-1][j]=0;
				for(l=1;l<i;l++){//shifting numbers
					arr[i-l][j]=arr[i-l-1][j];
					arr[i-l-1][j]=0;
				}
			}
		}
	}
	return score;//result of merged numbers
}
int left(int arr[][SIZE]){
	int i,j,l,score=0;
		for(i=0;i<SIZE;i++){//take numbers left
			for(j=SIZE-1;j>0;j--){
				if(arr[i][j-1]==0){
					arr[i][j-1]=arr[i][j];
					arr[i][j]=0;
					for(l=0;l<SIZE-1-j;l++){//shifting numbers
						arr[i][j+l]=arr[i][j+l+1];
						arr[i][j+l+1]=0;
					}
				}
			}
		}
	for(i=0;i<SIZE;i++){//merge numbers
		for(j=0;j<SIZE-1;j++){
			if(arr[i][j]==arr[i][j+1]){
				arr[i][j]*=2;
				score+=arr[i][j];
				arr[i][j+1]=0;
				for(l=0;l<SIZE-2-j;l++){//shifting numbers
					arr[i][j+l+1]=arr[i][j+l+2];
					arr[i][j+l+2]=0;
				}
			}
		}
	}
	return score;//result of merged numbers
}
int right(int arr[][SIZE]){
	int i,j,k,score=0;
		for(i=0;i<SIZE;i++){//take numbers right
			for(j=0;j<SIZE-1;j++){
				if(arr[i][j+1]==0){
					arr[i][j+1]=arr[i][j];
					arr[i][j]=0;
					for(k=0;k<j;k++){//shifting numbers
						arr[i][j-k]=arr[i][j-k-1];
						arr[i][j-k-1]=0;
					}
				}
			}
		}
	for(i=0;i<SIZE;i++){//merge numbers
		for(j=SIZE-1;j>0;j--){
			if(arr[i][j-1]==arr[i][j]){
				arr[i][j]*=2;
				score+=arr[i][j];
				arr[i][j-1]=0;
				for(k=0;k<j-1;k++){//shifting numbers
					arr[i][j-k-1]=arr[i][j-k-2];
					arr[i][j-k-2]=0;
				}
			}
		}
	}
	return score;//result of merged numbers
}
int compare(int arr1[][4],int arr2[][4]){//comparing two tables for checking something
	int count=0,i,j;
	for(i=0;i<4;i++){
		for(j=0;j<4;j++){
				if(arr1[i][j]!=arr2[i][j])
					return 0;
		}
	}
	return 1;
}
int check(int arr[][SIZE]){//check if there is any chance of wining the game
	int i,j,result=0;
	for(i=0;i<SIZE;i++){//check if there is any empty cells
		for(j=0;j<SIZE;j++){
			if(arr[i][j]==0)
				result++;
		}
	}
	for(i=0;i<SIZE;i++){//checking paired side cells
		for(j=0;j<SIZE-1;j++){
			if(arr[i][j]==arr[i][j+1])
				result++;
		}
	}
	for(i=0;i<SIZE-1;i++){//checking paired side cells
		for(j=0;j<SIZE;j++){
			if(arr[i][j]==arr[i+1][j])
				result++;
		}
	}
	return result;
}
void putRand(int arr[][SIZE],int RandNum){//putting random numbers in the table
	int i,j,k=0,iArr[17],jArr[17],count=0;
	srand(time(NULL));
	for(i=0;i<SIZE;i++){//finding empty cell for putting numbers in to them
		for(j=0;j<SIZE;j++){
			if(arr[i][j]==0){
				iArr[k]=i;
				jArr[k]=j;
				k++;
			}
		}
	}
		k=rand()%k;//selecting one of the empty cells randomly
		i=iArr[k];
		j=jArr[k];
		arr[i][j]=RandNum;
}
void firstTable(int arr[][SIZE]){//starting table
	int i,j;
	for(i=0;i<SIZE;i++){
		for(j=0;j<SIZE;j++){
				arr[i][j]=0;
		}
	}
	putRand(arr,2);//putting first number(2) in the random cell
	putRand(arr,2);//putting second number(2) in the random cell
}
int firstMenu(){//shownig the menu
	int order;
	while(1){
		printf("0 start the game\n1 how to play\n2 quit\n");
		scanf("%d",&order);
		if(order==1){//playing rules!
			printf("write orders to move the tiles. When two tiles with the same number touch, they merge into one!hope you enjoy :D\n");
		}
		else if(order==2)//quit order
			return 0;
		else if(order==0)//start order
			return 1;
		else{
			printf("invalid input!please try agian:\n");
			continue;
		}
	}
}
int pauseMenu(){
	int order;
	while(1){
		printf("0 resume\n1 new game\n2 quit\n");
		scanf("%d",&order);
		if(order==0)//resuming the game
			return 1;
		else if(order==1)//starting the  game from the first
			return 2;
		else if(order==2)//quit order
			return 0;
		else{
			printf("invalid input!please try agian:\n");
		}
	}
}
int winCheck(int arr[][SIZE]){//check if the player wins the game
	int i,j,result=0;
	for(i=0;i<SIZE;i++){//finding 2048 in the table
		for(j=0;j<SIZE;j++){
			if(arr[i][j]==2048)
				result++;
		}
	}
	return result;
}
int main(){
	int i,j,RandNum,board[SIZE][SIZE],temp[SIZE][SIZE],score=0,temp1,temp2;
	char order[6];
	srand(time(NULL));
	firstTable(board);//making the table and numbers to starting the game
	if(firstMenu()==0){//quit order
		return 0;
	}
	while(1){//the game will end in the while loop
		RandNum=rand()%3+2;
		if(RandNum==3){//increasing probability of number 2 (random number) from 1/2 to 2/3 and decreasing it for number 4 from 1/2 to 1/3
			RandNum=2;
		}
		printf("score:%d\n",score);//printing the score of the game before showing the table
		show(board);
		for(i=0;i<4;i++){//copying last board for compare and putting random number in the right way
			for(j=0;j<4;j++){
				temp[i][j]=board[i][j];
			}
		}
		scanf("%s",&order);//scaning order to do
		if(strcmp(order,"up")==0){
			score+=up(board);
		}
		else if(strcmp(order,"down")==0){
			score+=down(board);
		}
		else if(strcmp(order,"left")==0){
			score+=left(board);
		}	
		else if(strcmp(order,"right")==0){
			score+=right(board);
			}
		else if(strcmp(order,"exit")==0)
			return 0;
		else if(strcmp(order,"pause")==0){
			temp1=pauseMenu();
			if(temp1==0)//quit order
				return 0;
			else if(temp1==1)//resume order
				continue;
			else if(temp1==2){//new game order
				score=0;//put score 0 to starting the game from the first 
				firstTable(board);
				continue;
			}
		}
		else{
			printf("invalid input!please try agian:\n");
		}
		if(compare(temp,board)==0){//putting random number correctly
			putRand(board,RandNum);
		}
		if(winCheck(board)!=0){//win checker
			printf("score: %d\n",score);//showing winner score
			show(board);//showing the last table
			printf("!!!you win!!!\n");
			firstTable(board);
			temp2=firstMenu();
			if(temp2==0)
				return 0;
			score=0;//starting from the first
			continue;
		}
		if(check(board)==0){//lose checker
			printf("score: %d\n",score);//show loser score
			show(board);//show the last table
			printf("you lose!\n");
			firstTable(board);//starting from the first
			temp2=firstMenu();
			if(temp2==0)
				return 0;
			score=0;
			continue;
		}
	}
	return 0;
}
